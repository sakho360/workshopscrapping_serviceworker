/*
* Register the SW 
*/
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker
        .register('../sw_cached_site.js')
        .then(register => console.log('Service Worker: registered'))
        .catch(error => console.log(`Service Worker: error (${error})`));
    })
    console.log('Service Worker supported');
}